import machine as m
from utime import sleep
from microWebSrv import MicroWebSrv
import ujson as json
import ntptime
import clock
from networks import networks
from wifi import connect

UTC_OFFSET = 60 * 60 * 1  # GMT+1

ALARM_SEQUENCE = ((0.1, 0.1), (0.1, 0))
ARM_SEQUENCE = ((0.05, 0.05), (0.05, 0.05), (0.05, 0))
DISARM_SEQUENCE = ((0.05, 0.05), (0.05, 0.05), (0.2, 0))

PIN_SENSOR = 13
PIN_SENSOR_LED = 19
PIN_BUZZER = 22
PIN_STATUS_LED = 21

state = {
    'alarm_count': 0,
    'alarm_enabled': True,
}

pin_sensor = m.Pin(PIN_SENSOR, m.Pin.IN)

pin_led = m.Pin(PIN_SENSOR_LED, m.Pin.OUT)
pin_led.value(0)

pin_buzzer = m.Pin(PIN_BUZZER, m.Pin.OUT)
pin_buzzer.value(0)

pin_status = m.Pin(PIN_STATUS_LED, m.Pin.OUT)
pin_status.value(0)


def load_state():
    with open('state.json', 'r') as f:
        return json.loads(f.read())


def write_state():
    with open('state.json', 'w') as f:
        f.write(json.dumps(state))


wifi = connect(networks)

state = load_state()
boot_time = 0


rtc = clock.init(UTC_OFFSET)


def format_date(datetime):
    return '{0}-{1:02}-{2:02} {4:02}:{5:02}:{6:02}'.format(*datetime)


def beep_sequence(sequence):
    for item in sequence:
        pin_buzzer.value(1)
        sleep(item[0])
        pin_buzzer.value(0)
        sleep(item[1])


def on_movement(p):
    if p.value() == 1:
        print('on_movement')
        pin_led.value(1)

        if state['alarm_enabled']:
            print('beeping...')
            state['alarm_count'] += 1
            write_state()
            beep_sequence(ALARM_SEQUENCE)
    else:
        pin_led.value(0)


def _http_test(httpClient, httpResponse):
    content = """\
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CatAlarm</title>
    </head>
    <body>
        Client IP address = {}<br>
    </body>
</html>
    """.format(httpClient.GetIPAddr())

    httpResponse.WriteResponseOk(
        headers=None,
        contentType="text/html",
        contentCharset="UTF-8",
        content=content
    )


def _http_enable(httpClient, httpResponse):
    state['alarm_enabled'] = True
    beep_sequence(ARM_SEQUENCE)
    write_state()
    httpResponse.WriteResponseOk(content='ok')


def _http_disable(httpClient, httpResponse):
    state['alarm_enabled'] = False
    beep_sequence(DISARM_SEQUENCE)
    write_state()
    httpResponse.WriteResponseOk(content='ok')


def _http_status(httpClient, httpResponse):
    try:
        uptime = ntptime.time() - boot_time
    except Exception:
        uptime = 'error'

    httpResponse.WriteResponseJSONOk(obj={
        # 'machine_id': m.unique_id(),
        'uptime': uptime,
        'state': state,
        'rtc': format_date(rtc.datetime()),
    })


def _http_alarm_count(httpClient, httpResponse):
    httpResponse.WriteResponseJSONOk(obj={'count': state['alarm_count']})


def _http_setup_time(httpClient, httpResponse):
    time = rtc.update_time()
    httpResponse.WriteResponseOk(content=str(time))


srv = MicroWebSrv(routeHandlers=[
    ("/test", "GET", _http_test),
    ("/disable", "GET", _http_disable),
    ("/enable", "GET", _http_enable),
    ("/alarm_count", "GET", _http_alarm_count),
    ("/status", "GET", _http_status),
    ("/setup_time", "GET", _http_setup_time),
], webPath="/www")
srv.Start(threaded=True)

pin_sensor.irq(trigger=m.Pin.IRQ_RISING | m.Pin.IRQ_FALLING, handler=on_movement)

print('init done')
pin_status.value(1)
