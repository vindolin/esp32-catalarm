from machine import RTC
import ntptime
import utime as time

rtc = RTC()
utc_offset = 0


def update_time():
    # setup the realtime clock
    print('trying to fetch the time..')
    try:
        t = ntptime.time()
        globals()['boot_time'] = t
        tm = time.localtime(t + utc_offset)
        # convert the different format
        tm = tm[0:3] + tm[6:7] + tm[3:6] + (0,)
        rtc.datetime(tm)
    except Exception:  # timeout
        print('ran into a timeout :(')

    return rtc.datetime()


def init(utc_offset):
    globals()['utc_offset'] = utc_offset
    update_time()
    return rtc
